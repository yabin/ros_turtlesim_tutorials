/*! @file auto_move.h
    @author liuyabin
    @date Jan 16, 2019
    @brief the robot will clean the room automatically.

    @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_AUTO_MOVE_H_
#define GO_ROBOT_SRC_AUTO_MOVE_H_

#include "sub_modules/move_mode.h"
#include "sub_modules/state_check.h"

#endif /* GO_ROBOT_SRC_AUTO_MOVE_H_ */
