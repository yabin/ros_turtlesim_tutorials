// @copydoc Copyright

#include "basic_move.h"

void move(const ros::Publisher& velocity_publisher, double speed,
          double distance, bool is_forward) {
    geometry_msgs::Twist vel_msg;
    // set a random linear velocity in the x-axis
    if (is_forward)
       vel_msg.linear.x = std::abs(speed);
    else
       vel_msg.linear.x =-std::abs(speed);
    vel_msg.linear.y = 0;
    vel_msg.linear.z = 0;
    // set a random angular velocity in the y-axis
    vel_msg.angular.x = 0;
    vel_msg.angular.y = 0;
    vel_msg.angular.z = 0;

    double t0 = ros::Time::now().toSec();
    double current_distance = 0.0;
    ros::Rate loop_rate(100);
    do {
       velocity_publisher.publish(vel_msg);
       double t1 = ros::Time::now().toSec();
       current_distance = speed * (t1-t0);
       ros::spinOnce();
       loop_rate.sleep();
       // cout<<(t1-t0)<<", "<<current_distance <<", "<<distance<<endl;
    } while (current_distance < distance);
    vel_msg.linear.x = 0;
    velocity_publisher.publish(vel_msg);
}

void rotate(const ros::Publisher& publisher, double angular_speed,
             double relative_angle, bool clockwise) {
    geometry_msgs::Twist vel_msg;
    // set a random linear velocity in the x-axis
    vel_msg.linear.x = 0;
    vel_msg.linear.y = 0;
    vel_msg.linear.z = 0;
    // set a random angular velocity in the y-axis
    vel_msg.angular.x = 0;
    vel_msg.angular.y = 0;
    if (clockwise)
        vel_msg.angular.z =-std::abs(angular_speed);
    else
        vel_msg.angular.z = std::abs(angular_speed);
//   std::cout<<"vel_msg: "<<angular_speed<<std::endl;

    double t0 = ros::Time::now().toSec();
    double current_angle = 0.0;
    ros::Rate loop_rate(1000);
    do {
        publisher.publish(vel_msg);
        double t1 = ros::Time::now().toSec();
        current_angle = angular_speed * (t1-t0);
        ros::spinOnce();
        loop_rate.sleep();
        // std::cout<<(t1-t0)<<", "<<current_angle <<", "<<relative_angle<<std::endl;
    } while (current_angle < std::abs(relative_angle));
    vel_msg.angular.z = 0;
    publisher.publish(vel_msg);
}

void draw_circle(const ros::Publisher& publisher, double radius,
                 double angular_speed, double relative_angle, bool clockwise) {
    geometry_msgs::Twist vel_msg;
    // set a random linear velocity in the x-axis
    vel_msg.linear.x = radius*angular_speed;
    vel_msg.linear.y = 0;
    vel_msg.linear.z = 0;
    // set a random angular velocity in the y-axis
    vel_msg.angular.x = 0;
    vel_msg.angular.y = 0;
    if (clockwise)
        vel_msg.angular.z =-std::abs(angular_speed);
    else
        vel_msg.angular.z = std::abs(angular_speed);

    double t0 = ros::Time::now().toSec();
    double current_angle = 0.0;
    ros::Rate loop_rate(1000);
    do {
       publisher.publish(vel_msg);
       double t1 = ros::Time::now().toSec();
       current_angle = angular_speed * (t1-t0);
       ros::spinOnce();
       loop_rate.sleep();
       // std::cout<<(t1-t0)<<", "<<current_angle <<", "<<relative_angle<<std::endl;
    } while (current_angle < std::abs(relative_angle));
    vel_msg.angular.z = 0;
    vel_msg.linear.x = 0;
    publisher.publish(vel_msg);
}

void set_desired_orientation(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
    double desired_angle_radians) {
    ros::spinOnce();
    double relative_angle_radians = desired_angle_radians - current_pose.theta;
    // if we want to turn at a perticular orientation, we subtract the current orientation from it
    bool clockwise = ((relative_angle_radians < 0)?true:false);
    // std::cout<<desired_angle_radians <<","<<current_pose.theta<<","
    //    <<relative_angle_radians<<","<<clockwise<<std::endl;
    rotate(publisher, std::abs(relative_angle_radians),
            std::abs(relative_angle_radians), clockwise);
}

void move_goal(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
    const turtlesim::Pose& goal_pose, double distance_tolerance) {
    // We implement a Proportional Controller. We need to go from (x,y)
    // to (x',y'). Then, linear velocity v' = K ((x'-x)^2 + (y'-y)^2)^0.5
    // where K is the constant and ((x'-x)^2 + (y'-y)^2)^0.5 is
    // the Euclidian distance. The steering angle theta = tan^-1(y'-y)/(x'-x) is
    // the angle between these 2 points.
    geometry_msgs::Twist vel_msg;

    ros::Rate loop_rate(10);
    do {
        // linear velocity
        vel_msg.linear.x = 1.5*getDistance(current_pose.x, current_pose.y,
                                           goal_pose.x, goal_pose.y);
        vel_msg.linear.y = 0;
        vel_msg.linear.z = 0;
        // angular velocity
        vel_msg.angular.x = 0;
        vel_msg.angular.y = 0;
        vel_msg.angular.z = 4*(atan2(goal_pose.y - current_pose.y,
                                     goal_pose.x - current_pose.x)-current_pose.theta);

        publisher.publish(vel_msg);

        ros::spinOnce();
        loop_rate.sleep();
    } while (getDistance(current_pose.x, current_pose.y,
                         goal_pose.x, goal_pose.y) > distance_tolerance);
    std::cout << "end move goal" << std::endl;
    vel_msg.linear.x = 0;
    vel_msg.angular.z = 0;
    publisher.publish(vel_msg);
}
