/*! @file state_check.h
    @author liuyabin
    @date Jan 16, 2019
    @brief include functions about state check

    @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_SUB_MODULES_STATE_CHECK_H_
#define GO_ROBOT_SRC_SUB_MODULES_STATE_CHECK_H_


#include "ros/ros.h"

/*! @brief wait for starting of /turtlesim node.

    @param [in] time Max wait time. t = 0.5*time seconds.
    @return if /turtlesim node is running
    @retval false until time out /turtlesim_node is not running
 */
bool wait_turtlesim_start(int time);

/*! @brief wait for publishing the topic

    @param [in] topic_name The name of the topic
    @param [in] time Max wait time. t = 0.5*time seconds.
    @return whether the topic is published
    @retval false until time out the topic is not published
 */
bool wait_topic_publish(std::string topic_name, int time);

/*! @brief wait for setting of the parameter

    @param [in] n Current node handle,we can get parameter through the node handle
    @param [in] param_name The name of the parameter
    @param [in] time Max wait time. t = 0.5*time seconds.
    @return whether the parameter is published
    @retval false until time out the parameter is not set
 */
bool wait_param_set(const ros::NodeHandle& n, std::string param_name, int time);


#endif /* GO_ROBOT_SRC_SUB_MODULES_STATE_CHECK_H_ */
