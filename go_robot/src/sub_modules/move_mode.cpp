// @copydoc Copyright

#include "move_mode.h"

void grid_clean(const ros::Publisher& publisher, const turtlesim::Pose& current_pose) {
    ros::Rate loop(0.5);
    turtlesim::Pose goal_pose;
    goal_pose.x = 1;
    goal_pose.y = 1;
    goal_pose.theta = 0;
    move_goal(publisher, current_pose, goal_pose, 0.01);
    loop.sleep();
    set_desired_orientation(publisher, current_pose, 0);
    loop.sleep();

    move(publisher, 3, 9, true);
    loop.sleep();
    rotate(publisher, degrees2radians(90), degrees2radians(90), false);
    loop.sleep();
    move(publisher, 3, 9, true);

    int grid_num = 4;
    for (int i = 0; i < grid_num; ++i) {
        rotate(publisher, degrees2radians(90), degrees2radians(90), false);
        loop.sleep();
        move(publisher, 3, 1, true);
        rotate(publisher, degrees2radians(90), degrees2radians(90), false);
        loop.sleep();
        move(publisher, 3, 9, true);

        rotate(publisher, degrees2radians(90), degrees2radians(90), true);
        loop.sleep();
        move(publisher, 3, 1, true);
        rotate(publisher, degrees2radians(90), degrees2radians(90), true);
        loop.sleep();
        move(publisher, 3, 9, true);
    }

    // double distance = getDistance(turtlesim_pose.x, turtlesim_pose.y, x_max
}

void spiral_clean(const ros::Publisher& publisher, const turtlesim::Pose& current_pose) {
    geometry_msgs::Twist vel_msg;
    double count = 0;

    double constant_speed = 5;
    double vk = 1;
    double wk = 2;
    double rk = 0.5;
    ros::Rate loop(1);

    turtlesim::Pose goal_pose;
    goal_pose.x = 5.54444;
    goal_pose.y = 5.54444;
    ros::spinOnce();
    move_goal(publisher, current_pose, goal_pose, 0.01);
    ros::spinOnce();
    set_desired_orientation(publisher, current_pose, 0);
    ros::spinOnce();

    do {
        rk = rk + 0.75;
        vel_msg.linear.x = rk;
        vel_msg.linear.y = 0;
        vel_msg.linear.z = 0;

        vel_msg.angular.x = 0;
        vel_msg.angular.y = 0;
        vel_msg.angular.z = constant_speed;

        std::cout << "vel_msg.linear.x = " << vel_msg.linear.x << std::endl;
        std::cout << "vel_msg.angular.z = " << vel_msg.angular.z << std::endl;
        publisher.publish(vel_msg);
        ros::spinOnce();

        loop.sleep();
        std::cout << rk << " , " << vk << " , " << wk << std::endl;
    } while ((current_pose.x < 8.5) && (current_pose.y < 8.5));
    vel_msg.linear.x = 0;
    vel_msg.angular.z = 0;
    publisher.publish(vel_msg);
}

bool ld_mode_d(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
               ros::NodeHandle* n) {
    turtlesim::Pose goal;
    goal.x = 4;
    goal.y = 3;
    move_goal(publisher, current_pose, goal, 0.01);

    ros::ServiceClient clearClient = n->serviceClient<std_srvs::Empty>("/clear");
    std_srvs::Empty clearSrv;
    if (!clearClient.call(clearSrv)) {
        ROS_INFO("Failed to call service '/clear'");
        return false;
    }

    set_desired_orientation(publisher, current_pose, 0);

    ros::ServiceClient spawnClient = n->serviceClient<turtlesim::Spawn>("/spawn");
    turtlesim::Spawn spawnSrv;
    spawnSrv.request.x = 5.5;
    spawnSrv.request.y = 9.5;
    spawnSrv.request.theta = degrees2radians(-90);
    spawnSrv.request.name = "turtle2";
    if (!spawnClient.call(spawnSrv)) {
        ROS_INFO("Failed to call service '/clear'");
        return false;
    }
    ROS_INFO("New turtle name is %s", spawnSrv.response.name.c_str());
    n->setParam("spawned_turtle2", true);

    move(publisher, 1, 1, 1);
    draw_circle(publisher, 2.5, 1, PI, false);
    move(publisher, 1, 1, 1);
    rotate(publisher, PI/2, PI/2, 0);
    move(publisher, 2, 5, 1);

    return true;
}

bool ld_mode_l(const ros::Publisher& publisher) {
    move(publisher, 1, 4.5, 1);
    rotate(publisher, degrees2radians(45), degrees2radians(90), 0);
    move(publisher, 2, 3.5, 1);

    return true;
}
