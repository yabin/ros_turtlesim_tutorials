// @copydoc Copyright

#include "state_check.h"

bool wait_turtlesim_start(int time) {
    // wait for /turtlesim node start.Over time is 5 seconds
    ros::Rate r(2);     // 2 Hz
    ros::V_string all_runing_nodes;
    bool overtime = true;
    for (int i = 0; i < time; ++i) {
        ros::master::getNodes(all_runing_nodes);
        for (ros::V_string::iterator iter = all_runing_nodes.begin();
                iter != all_runing_nodes.end(); ++iter) {
//          std::cout<<*iter<<std::endl;
            if (*iter == "/turtlesim") {
                // check if there is a topic named '/turtle1/pose'
                bool is_exist = false;
                ros::master::V_TopicInfo master_topics;
                ros::master::getTopics(master_topics);
                for (ros::master::V_TopicInfo::iterator it =
                        master_topics.begin(); it != master_topics.end();
                        ++it) {
                    if (it->name == "/turtle1/pose") {
                        is_exist = true;
                        break;
                    }
                    //      std::cout<<it->name<<", "<<it->datatype<<std::endl;
                }
                if (!is_exist) {
                    ROS_INFO("\n ******** ERROR *********\n");
                    ROS_INFO(
                            "'/turtlesim' node is running,but not found 'turtle1/pose' topic.");
                    return false;
                }
                return true;
            }
        }
        r.sleep();
    }

    ROS_INFO("\n ******** ERROR *********\n");
    ROS_INFO("time out! '/turtlesim' node is not running.");
    return false;
}

bool wait_topic_publish(std::string topic_name, int time) {
    ros::Rate r(2);     // 2 Hz
    // check if there is a topic named 'topic_name'
    ros::master::V_TopicInfo master_topics;
    for (int i = 0; i < time; ++i) {
        ros::master::getTopics(master_topics);
        for (ros::master::V_TopicInfo::iterator it = master_topics.begin();
                it != master_topics.end(); ++it) {
            if (it->name == topic_name) {
                return true;
            }
            //      std::cout<<it->name<<", "<<it->datatype<<std::endl;
        }
        r.sleep();
    }
    ROS_INFO("time out! '/turtle2/pose' topic is not found.");
    return false;
}

bool wait_param_set(const ros::NodeHandle& n, std::string param_name, int time) {
    ros::Rate r(2);     // 2 Hz
    for (int i = 0; i < time; ++i) {
        if (n.hasParam(param_name))
            return true;
        r.sleep();
    }
    return false;
}

