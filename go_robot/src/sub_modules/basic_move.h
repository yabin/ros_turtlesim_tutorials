/*! @file basic_move.h
    @author liuyabin
    @date Jan 14, 2019
    @brief include functions to move turtle through publishing topic

    @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_SUB_MODULES_BASIC_MOVE_H_
#define GO_ROBOT_SRC_SUB_MODULES_BASIC_MOVE_H_

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include <sstream>
#include <cmath>
#include "turtlesim/Pose.h"

const double PI = 3.14159265359;

inline double degrees2radians(double angle_in_degrees) {
    return angle_in_degrees *PI /180.0;
}

/*! @brief move robot straight

    make the robot move with a certain linear velocity for a
    certain distance in a forward or backward straight direction.
    @param [in] velocity_publisher Publisher to the turtle,message type is geometry_msgs::Twist
    @param [in] speed velocity
    @param [in] distance distance of moving
    @param [in] is_forward if ture,move turtle forward
*/
void move(const ros::Publisher& velocity_publisher, double speed,
          double distance, bool is_forward);

/*! @brief rotate the robot at specified angle from its current angle

    @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
    @param [in] angular_speed robote's angular speed(rad/s)
    @param [in] relative_angle total angle to rotate(rad)
    @param [in] clockwise if ture,rotate the turtle clockwise
*/
void rotate(const ros::Publisher& publisher, double angular_speed,
             double relative_angle, bool clockwise);

/*! @brief move and draw a circle or arc

    @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
    @param [in] radius The radius of rotating
    @param [in] angular_speed Robote's angular speed(rad/s)
    @param [in] relative_angle Total angle to rotate(rad)
    @param [in] clockwise If ture,rotate the turtle clockwise
*/

void draw_circle(const ros::Publisher& publisher, double radius,
                 double angular_speed, double relative_angle = 2*PI,
                 bool clockwise = true);

/*! @brief rotate the robot at an absolute angle,whatever its current angle is

    @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
    @param [in] current_pose Current robot pose,message type is turtlesim::Pose
    @param [in] desired_angle_radians The desired rotate angle
*/
void set_desired_orientation(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
    double desired_angle_radians);

inline double getDistance(double x1, double y1, double x2, double y2) {
    return sqrt(pow((x2-x1), 2) + pow((y2-y1), 2));
}

/*! @brief move robot to a certain position

    move robot to a certain position of 2D space.We implement a proportional controller,that is:
        v' = K*E_pos
    where E_pos is distance between current position and goal.
    You must give a distance tolerance,and when E_pos smaller than the tolerance,the robot will stop.
    There is complete formula to decision v' and theta:
        v' = K ((x'-x)^2 + (y'-y)^2)^0.5
        theta = tan^-1(y'-y)/(x'-x)
    where (x,y) is current position,(x',y') is destination
    @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
    @param [in] current_pose Current robot pose,message type is turtlesim::Pose
    @param [in] goal_pose The position of destination
    @param [in] distance_tolerance The tolerant distance error
*/
void move_goal(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
    const turtlesim::Pose& goal_pose, double distance_tolerance);

#endif /* GO_ROBOT_SRC_SUB_MODULES_BASIC_MOVE_H_ */
