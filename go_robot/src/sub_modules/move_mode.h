/*! @file move_mode.h
 @author liuyabin
 @date Jan 16, 2019
 @brief robot move mode

 @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_SUB_MODULES_MOVE_MODE_H_
#define GO_ROBOT_SRC_SUB_MODULES_MOVE_MODE_H_

#include "basic_move.h"
#include "std_srvs/Empty.h"
#include "turtlesim/Spawn.h"

/*! @brief move robot in grid mode

 @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
 @param [in] current_pose Current robot pose,message type is turtlesim::Pose
 */
void grid_clean(const ros::Publisher& publisher, const turtlesim::Pose& current_pose);

/*! @brief move robot in spiral mode

 @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
 @param [in] current_pose Current robot pose,message type is turtlesim::Pose
 */
void spiral_clean(const ros::Publisher& publisher, const turtlesim::Pose& current_pose);

/*! @brief plot 'D' of LD mode

 @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
 @param [in] current_pose Current robot pose,message type is turtlesim::Pose
 @param [in] n Current node handle,we can get service list through the node handle
 @return Whether it is succeed
 @retval false Something goes wrong
 */

bool ld_mode_d(const ros::Publisher& publisher, const turtlesim::Pose& current_pose,
               ros::NodeHandle* n);

/*! @brief plot 'L' of LD mode

 @param [in] publisher Publisher to the turtle,message type is geometry_msgs::Twist
 */

bool ld_mode_l(const ros::Publisher& publisher);

#endif /* GO_ROBOT_SRC_SUB_MODULES_MOVE_MODE_H_ */
