// @copydoc Copyright

#include "move_robot.h"

/*! @brief subscribe robot position

    in order to rotate the robot at an absolute angle,you need to get robot position all the time

    @param [in] pose_message Current position of the robot published by the robot
 */
void pose_callback(const turtlesim::Pose::ConstPtr& pose_message);

turtlesim::Pose turtlesim_pose;     // current robot pose

int main(int argc, char **argv) {
    using std::cout;
    using std::cin;
    using std::endl;
    // Initiate new ROS node named "robot_cleaner"
    ros::init(argc, argv, "robot_cleaner");
    ros::NodeHandle n;

    ros::Publisher publisher = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);
    ros::Subscriber pose_subscriber = n.subscribe("/turtle1/pose", 10, pose_callback);

    bool is_continue = true;
    char mode;

    while (is_continue && (ros::ok())) {
        ROS_INFO("\n\n\n ******** START TESTING *********\n");

        std::cout << "Which action do you want to excute?" << std::endl;
        std::cout << "Enter 'm' for moving robot." << std::endl;
        std::cout << "Enter 'r' for rotating robot." << std::endl;
        std::cout << "Enter 'd' for drawing a circle." << std::endl;
        std::cout << "Enter 'g' for moving robot to a goal." << std::endl;
        std::cout << "Enter 's' for setting a certain angle" << std::endl;
        std::cin >> mode;

        switch (std::tolower(mode)) {
            case 'm': {
                double speed, distance;
                bool is_forward;
                std::cout << "Enter speed: ";
                std::cin >> speed;
                std::cout << "Enter distance: ";
                std::cin >> distance;
                std::cout << "Forward?: ";
                std::cin >> is_forward;
                move(publisher, speed, distance, is_forward);
                ros::spinOnce();
                break;
            }
            case 'r': {
                double angular_speed, angle;
                bool is_clockwise;
                cout << "Enter angular velocity(degree/s): ";
                cin >> angular_speed;
                cout << "Enter angle(degree): ";
                cin >> angle;
                cout << "Clockwise?: ";
                cin >> is_clockwise;
                rotate(publisher, degrees2radians(angular_speed),
                        degrees2radians(angle), is_clockwise);
                break;
            }
            case 'd': {
                double radius, angular_vel, relative_angle;
                bool clockwise;
                cout << "Enter radius: ";
                cin >> radius;
                cout << "Enter angular velocity(degree/s): ";
                cin >> angular_vel;
                cout << "Enter total angle(degree): ";
                cin >> relative_angle;
                cout << "Clockwise?";
                cin >> clockwise;
                draw_circle(publisher, radius, degrees2radians(angular_vel),
                        degrees2radians(relative_angle), clockwise);
                break;
            }
            case 'g': {
                turtlesim::Pose goal_pose;
                cout << "enter x position: ";
                cin >> goal_pose.x;
                cout << "enter y position: ";
                cin >> goal_pose.y;
                goal_pose.theta = 0;
                ros::spinOnce();
                move_goal(publisher, turtlesim_pose, goal_pose, 0.01);
                break;
            }
            case 's': {
                double abs_angle;
                cout << "enter absolute angle(degree): ";
                cin >> abs_angle;
                ros::spinOnce();    // make sure callback function be called
                set_desired_orientation(publisher, turtlesim_pose, degrees2radians(abs_angle));
                break;
            }
            default: {
                cout << "Unknown action command!" << endl;
                break;
            }
        }

        cout << "move again?";
        cin >> is_continue;
    }
}

void pose_callback(const turtlesim::Pose::ConstPtr & pose_message) {
    turtlesim_pose.x = pose_message->x;
    turtlesim_pose.y = pose_message->y;
    turtlesim_pose.theta = pose_message->theta;
}
