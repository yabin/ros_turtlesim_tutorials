/*! @file ld_mode_l.h
    @author liuyabin
    @date Jan 18, 2019
    @brief plot L of LD mode

    @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_LD_MODE_L_H_
#define GO_ROBOT_SRC_LD_MODE_L_H_


#include "sub_modules/state_check.h"
#include "sub_modules/move_mode.h"


#endif /* GO_ROBOT_SRC_LD_MODE_L_H_ */
