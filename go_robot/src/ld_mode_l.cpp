// @copydoc Copyright

#include "ld_mode_l.h"

/*! @brief subscribe robot position

 in order to rotate the robot at an absolute angle,you need to get robot position all the time

 @param [in] pose_message Current position of the robot published by the robot
 */
void pose_callback(const turtlesim::Pose::ConstPtr& pose_message);

turtlesim::Pose turtlesim_pose;     // maintain current robot pose

int main(int argc, char** argv) {
    // Initiate new ROS node named "robot_cleaner"
    ros::init(argc, argv, "robot_cleaner");
    ros::NodeHandle n;

    if (!wait_topic_publish("/turtle2/pose", 20))
        return 1;
    if (!wait_param_set(n, "spawned_turtle2", 20))
        return 1;
    n.deleteParam("spawned_turtle2");
    ros::Publisher publisher = n.advertise<geometry_msgs::Twist>(
            "/turtle2/cmd_vel", 1000);
    ros::Subscriber pose_subscriber = n.subscribe("/turtle2/pose", 10,
                                                  pose_callback);

    ld_mode_l(publisher);
}

void pose_callback(const turtlesim::Pose::ConstPtr & pose_message) {
    turtlesim_pose.x = pose_message->x;
    turtlesim_pose.y = pose_message->y;
    turtlesim_pose.theta = pose_message->theta;
}
