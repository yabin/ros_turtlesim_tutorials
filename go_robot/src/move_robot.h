/*! @file move_robot.h
    @author liuyabin
    @date Jan 14, 2019
    @brief basic functions of move robot

    @copydoc Copyright
 */
#ifndef GO_ROBOT_SRC_MOVE_ROBOT_H_
#define GO_ROBOT_SRC_MOVE_ROBOT_H_

#include "sub_modules/basic_move.h"



#endif /* GO_ROBOT_SRC_MOVE_ROBOT_H_ */
