// @copydoc Copyright

#include "auto_move.h"

/*! @brief subscribe robot position

    in order to rotate the robot at an absolute angle,you need to get robot position all the time

    @param [in] pose_message Current position of the robot published by the robot
 */
void pose_callback(const turtlesim::Pose::ConstPtr& pose_message);

turtlesim::Pose turtlesim_pose;     // maintain current robot pose

int main(int argc, char** argv) {
    // Initiate new ROS node named "robot_cleaner"
    ros::init(argc, argv, "robot_cleaner");
    ros::NodeHandle n;

    if (!wait_turtlesim_start(10))
        return 1;

    ros::Publisher publisher = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);
    ros::Subscriber pose_subscriber = n.subscribe("/turtle1/pose", 10, pose_callback);

    bool is_continue = true;
    char mode;
    while (is_continue && ros::ok()) {
        if (argc == 1) {
            ROS_INFO("\n\n\n ******** START TESTING *********\n");
            std::cout << "clean the room in grid mode or spiral mode?" << std::endl;
            std::cout << "enter 'g' for gird mode,enter 's' for spiral mode: ";
            std::cin >> mode;
        } else if (argc == 2) {
            mode = argv[1][0];
            is_continue = false;
        }
        switch (std::tolower(mode)) {
            case 'g':
                grid_clean(publisher, turtlesim_pose);
                break;
            case 's':
                spiral_clean(publisher, turtlesim_pose);
                break;
            default:
                break;
        }

        if (argc == 1) {
            std::cout << "move again?: ";
            std::cin >> is_continue;
        }
    }
}

void pose_callback(const turtlesim::Pose::ConstPtr & pose_message) {
    turtlesim_pose.x = pose_message->x;
    turtlesim_pose.y = pose_message->y;
    turtlesim_pose.theta = pose_message->theta;
}
