# ros_turtlesim_tutorials

这个教程演示了如何使用 turtlesim 来模拟一个扫地机器人，面向对象为 ROS 的初学者。[turtlesim](http://wiki.ros.org/turtlesim)  包的简介中有关于该代码的视频介绍，但是视频似乎并不完整。我在 [turtlesim_cleaner](https://github.com/zshn25/turtlesim_cleaner) 里找到了视频中的源码。这里对该源码进行重构，提高了代码质量，并增了了一部分内容。

# 1 环境

本代码使用的环境为 Ubuntu16.04, ROS 的版本为 [ros-kinetic-desktop-full](http://wiki.ros.org/kinetic/Installation/Ubuntu) （包括 turtlesim 包）。

# 2 使用

## 2.1 编译

假设你的 catkin 工作空间为 `~/catkin_ws` ，所有包都放置在 `~/catkin_ws/src` 文件夹下（如果你之前跟随了官方教程，应该满足这个条件）。

```bash
# clone the repo in the folder wherever you want 
$ git clone git@gitlab.com:yabin/ros_turtlesim_tutorials.git
# copy go_robot package to the folder you place the packages
$ cp -a ros_turtlesim_tutorials/go_robot ~/catkin_ws/src/go_robot
# build your packages
$ cd ~/catkin_ws
$ catkin_make
$ source devel/setup.bash
```

## 2.2 基本移动控制

使用：

```bash
# terminal 1
$ roscore

# terminal 2
$ rosrun turtlesim turtlesim_node

# terminal 3
$ rosrun go_robot move_robot
```

或者直接运行 launch 文件：

```bash
# terminal 1
$ roscore

# terminal 2
$ roslaunch go_robot move_robot.launch
```

然后根据交互式的提示我们就可以控制机器人进行基本的移动.

![move_robot](./materials/demo.gif)

## 2.3 指定工作模式运行

  使用：

```bash
# terminal 1
$ roscore

# terminal 2
$ rosrun turtlesim turtlesim_node

# terminal 3
$ rosrun go_robot auto_move
```

或者直接运行 launch 文件：

```bash
# terminal 1
$ roscore

# terminal 2
$ roslaunch go_robot auto_move.launch mode:=g
```

`mode:=g` 表示以网格(grid)模式运行，你也可以使用 `mode:=s` 或者省略该参数以螺旋模式(spiral)运行。

![grid](materials/grid.gif)

![spiral](materials/spiral.gif)

## 2.4 多节点协同工作

LD 模式使用了两个机器人同时绘制 L 和 D 两个字母：

```shell
# terminal 1
$ roscore

# terminal 2
$ roslaunch go_robot ld_mode.launch
```

![ld_mode](materials/ld_mode.gif)

  

# 3 教程

你可以在 wiki 中查看关于本代码的教程。本教程中不仅仅介绍了如何控制机器人。同时也介绍了一些编码的软技能，如使用 google c++ 代码规范、使用 doxgen 自动生成文档、配置 eclipse 代码模板等。本教程基本覆盖了 ROS 中的所有基本元素的使用，包括但不限于：

*  话题的发布
*  话题的订阅
*  查看当前运行节点
*  查看当前运行话题
*  使用服务
*  使用 roslaunch 启动多个节点

如果你之前对 ROS 并没有任何了解，而仅仅是刚安装完 ROS 想找一些初学者的教程。强烈建议你在学习本教程之前首先根据 ROS 的 [官方教程](http://wiki.ros.org/ROS/Tutorials) 先了解一下 ROS 中的一些基本概念。